﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using System.Diagnostics;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Annotations;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace EHMonitor
{
    class EHVoltageView : EHPlotView
    {
        TextAnnotation annotation;

        LinearAxis xAxis;
        LinearAxis uAxis;
        LineSeries voltageSeries;
        EHBuffer voltageSource;

        public string Annotation;

        public void Add( int voltage )
        {
            voltageSource.Push( voltage );
        }

        public void Invalidate()
        {
            annotation.Text = Annotation;

            Model.InvalidatePlot( true );
        }

        public EHVoltageView( Window window, int size ) : base( window )
        {
            voltageSource = new EHBuffer( size );
        }

        protected override void OnLoad( Context context )
        {
            base.OnLoad( context );

            Model.Title = "Storage";
            Model.IsLegendVisible = false;
            Model.SubtitleColor = Style.Colors.Accent.ToOxy();
            Model.DefaultFont = "Source Code Pro";
            Model.Background = Style.Colors.Background.ToOxy();
            Model.TextColor = Style.Colors.Foreground.ToOxy();

            annotation = new TextAnnotation();
            annotation.TextPosition = new DataPoint( 0, 4000 );
            Annotation = "Power available\n\n10 mW for 12.5s\n500 mW for 0.1s\n10 mA for 0.4s";
            annotation.Text = Annotation;
            annotation.TextVerticalAlignment = VerticalAlignment.Top;
            annotation.TextHorizontalAlignment = HorizontalAlignment.Left;
            annotation.StrokeThickness = 0;

            xAxis = new LinearAxis();
            xAxis.TickStyle = TickStyle.None;
            xAxis.Position = AxisPosition.Bottom;
            xAxis.MajorGridlineStyle = LineStyle.Solid;
            xAxis.MajorStep = 128;
            xAxis.ClipTitle = false;
            xAxis.Font = "Source Code Pro";
            xAxis.TextColor = Color.White.ToOxy();
            xAxis.LabelFormatter = new Func<double, string>( ( d ) => string.Empty );

            uAxis = new LinearAxis();
            uAxis.Key = "u";
            uAxis.TextColor = Color.CadetBlue.ToOxy();
            uAxis.TickStyle = TickStyle.Inside;
            uAxis.Position = AxisPosition.Right;
            uAxis.MajorGridlineStyle = LineStyle.Solid;
            uAxis.MinorGridlineStyle = LineStyle.None;
            uAxis.MinorStep = 200;
            uAxis.MajorStep = 400;
            uAxis.Minimum = -20;
            uAxis.Maximum = 4200 + 20;

            voltageSeries = new LineSeries();
            voltageSeries.ItemsSource = voltageSource;
            voltageSeries.Color = Color.CadetBlue.ToOxy();
            voltageSeries.Title = "Voltage (mV)";
            voltageSeries.LineStyle = LineStyle.Solid;
            voltageSeries.YAxisKey = uAxis.Key;

            Model.Axes.Add( xAxis );
            Model.Axes.Add( uAxis );
            Model.Annotations.Add( annotation );
            Model.Series.Add( voltageSeries );
            Model.InvalidatePlot( true );
        }
    }

    class EHPowerView : EHPlotView
    {
        LinearAxis xAxis;

        LinearAxis pAxis;
        LinearAxis uiAxis;

        LineSeries powerSeries;
        LineSeries currentSeries;
        LineSeries voltageSeries;

        EHBuffer powerSource;
        EHBuffer currentSource;
        EHBuffer voltageSource;

        public string Title
        {
            get { return Model.Title; }
            set { Model.Title = value; }
        }

        public string PowerLegend
        {
            get { return powerSeries.Title; }
            set { powerSeries.Title = value; }
        }

        public string VoltageLegend
        {
            get { return voltageSeries.Title; }
            set { voltageSeries.Title = value; }
        }

        public string CurrentLegend
        {
            get { return currentSeries.Title; }
            set { currentSeries.Title = value; }
        }

        public void Add( int power, int current, int voltage )
        {
            powerSource.Push( power );
            currentSource.Push( current );
            voltageSource.Push( voltage );
        }

        public void Invalidate()
        {
            powerSeries.Title = $"Power   {powerSource.Average.ToString( "D4" )} uW";
            voltageSeries.Title = $"Voltage {voltageSource.Average.ToString( "D4" )} mV";
            currentSeries.Title = $"Current {currentSource.Average.ToString( "D4" )} uA";

            Model.InvalidatePlot( true );
        }

        public EHPowerView( Window window, int size ) : base( window )
        {
            powerSource = new EHBuffer( size );
            currentSource = new EHBuffer( size );
            voltageSource = new EHBuffer( size );

            powerSource.Calculations = true;
            currentSource.Calculations = true;
            voltageSource.Calculations = true;
        }

        protected override void OnLoad( Context context )
        {
            base.OnLoad( context );

            Model.IsLegendVisible = true;
            Model.SubtitleColor = Style.Colors.Accent.ToOxy();
            Model.DefaultFont = "Source Code Pro";
            Model.LegendTitle = "Average";
            Model.Background = Style.Colors.Background.ToOxy();
            Model.TextColor = Style.Colors.Foreground.ToOxy();

            xAxis = new LinearAxis();
            xAxis.TickStyle = TickStyle.None;
            xAxis.Position = AxisPosition.Bottom;
            xAxis.LabelFormatter = new Func<double, string>( ( d ) => string.Empty );

            pAxis = new LinearAxis();
            pAxis.TextColor = Color.IndianRed.ToOxy();
            pAxis.TickStyle = TickStyle.Inside;
            pAxis.Position = AxisPosition.Left;
            pAxis.MajorGridlineStyle = LineStyle.Solid;
            pAxis.MinorGridlineStyle = LineStyle.None;
            pAxis.MinorStep = 100;
            pAxis.MajorStep = 250;
            pAxis.Minimum = -10;
            pAxis.Maximum = 5000 + 10;

            uiAxis = new LinearAxis();
            uiAxis.Key = "ui";
            uiAxis.TextColor = Color.CadetBlue.ToOxy();
            uiAxis.TickStyle = TickStyle.Inside;
            uiAxis.Position = AxisPosition.Right;
            uiAxis.MajorGridlineStyle = LineStyle.Solid;
            uiAxis.MinorGridlineStyle = LineStyle.None;
            uiAxis.MinorStep = 100;
            uiAxis.MajorStep = 250;
            uiAxis.Minimum = -10;
            uiAxis.Maximum = 5000 + 10;

            powerSeries = new LineSeries();
            powerSeries.StrokeThickness = 4;
            powerSeries.ItemsSource = powerSource;
            powerSeries.Color = Color.IndianRed.ToOxy();
            powerSeries.Title = "Power   (uW)";
            powerSeries.LineStyle = LineStyle.Solid;

            voltageSeries = new LineSeries();
            voltageSeries.ItemsSource = voltageSource;
            voltageSeries.Color = Color.CadetBlue.ToOxy();
            voltageSeries.Title = "Voltage (mV)";
            voltageSeries.LineStyle = LineStyle.Solid;
            voltageSeries.YAxisKey = uiAxis.Key;

            currentSeries = new LineSeries();
            currentSeries.ItemsSource = currentSource;
            currentSeries.Color = Color.CadetBlue.ToOxy();
            currentSeries.Title = "Current (uA)";
            currentSeries.LineStyle = LineStyle.Solid;
            currentSeries.YAxisKey = uiAxis.Key;

            Model.Axes.Add( xAxis );
            Model.Axes.Add( pAxis );
            Model.Axes.Add( uiAxis );
            Model.Series.Add( powerSeries );
            Model.Series.Add( currentSeries );
            Model.Series.Add( voltageSeries );
            Model.InvalidatePlot( true );
        }
    }

    class EHBuffer : IEnumerable<DataPoint>
    {
        readonly int[] buffer;
        readonly int bufferLengthMask;
        int head;

        int maxValue;
        int averageValue;

        public bool Calculations;
        public int Max => maxValue;
        public int Average => averageValue;

        public EHBuffer( int size )
        {
            buffer = new int[ size ];
            bufferLengthMask = ~size;
        }

        public void Push( int data )
        {
            buffer[ head ] = data;
            head = (head + 1) & bufferLengthMask;

            if ( Calculations )
            {
                maxValue = 0;
                averageValue = 0;

                for ( var i = 0; i < buffer.Length; i++ )
                {
                    var value = buffer[ i ];

                    averageValue += value;
                    maxValue = Math.Max( maxValue, value );
                }

                averageValue = averageValue / buffer.Length;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<DataPoint> GetEnumerator()
        {
            for ( var i = 0; i < buffer.Length; i++ )
            {
                yield return new DataPoint( i, buffer[ (head + i) & bufferLengthMask ] );
            }
        }
    }

    class EHWindow : Window
    {
        Button exportButton;
        Button exportDisplayButton;
        Button auxPowerButton;

        Timer serialTimer;
        SerialPort serialPort;

        EHVoltageView storageView;

        EHPowerView harvesterView;
        EHPowerView sensorView;

        readonly byte[] serialWriteDriveON = new byte[] { 0x01, 0x01 };
        readonly byte[] serialWriteDriveOFF = new byte[] { 0x01, 0x00 };

        readonly byte[] serialWrite = new byte[] { 0x0, 0x0 };
        readonly byte[] serialReadBuffer = new byte[8];
        readonly int[] serialNumericBuffer = new int[5];

        static readonly int sensorBufferAverages = 20;
        float[][] sensorBuffer = new float[3][] { new float[ sensorBufferAverages ], new float[ sensorBufferAverages ], new float[ sensorBufferAverages ] };
        int sensorBufferHead = 0;

        static readonly int harvesterBufferAverages = 20;
        float[][] harvesterBuffer = new float[3][] { new float[ harvesterBufferAverages ], new float[ harvesterBufferAverages ], new float[ harvesterBufferAverages ] };
        int harvesterBufferHead = 0;

        bool sentDriving = false;
        bool driving = false;

        int storageUpdates;
        int storageUpdateCounter;

        void OnPlotTimer()
        {
            if ( !sentDriving )
            {
                sentDriving = true;

                if ( driving )
                    serialPort.Write( serialWriteDriveON, 0, serialWriteDriveON.Length );
                else
                    serialPort.Write( serialWriteDriveOFF, 0, serialWriteDriveOFF.Length );
            }

            serialPort.Write( serialWrite, 0, serialWrite.Length );
        }

        bool IsPortOpen()
        {
            return (serialPort != null && serialPort.IsOpen);
        }

        void OpenPort()
        {
            ClosePort();

            var serialPortNames = SerialPort.GetPortNames();
            serialPortNames.Contains( Properties.Settings.Default.SerialPortName );

            serialPort = new SerialPort( Properties.Settings.Default.SerialPortName, 115200, Parity.None, 8, StopBits.One );
            serialPort.ReceivedBytesThreshold = serialReadBuffer.Length;
            serialPort.DataReceived += OnSerialPort;
            serialPort.Open();

            serialTimer.Start();
        }

        void ClosePort()
        {
            if ( IsPortOpen() )
            {
                serialPort.Close();
            }
        }

        public EHWindow( WindowManager manager, WindowSettings settings ) : base( manager, settings )
        {
            exportButton = new Button( this );
            exportButton.Text = "EXPORT";
            exportButton.TextStyle = new TextStyle( "Source Code Pro", 12, FontWeight.Bold );
            exportButton.OnClick += ExportButton_OnClick;
            exportButton.ToggleOnClick = true;

            exportDisplayButton = new Button( this );
            exportDisplayButton.Text = "0 points in memory";
            exportDisplayButton.TextAlignment = new TextStyleAlignment( TextAlignment.Center, TextAlignment.Leading );
            exportDisplayButton.TextStyle = new TextStyle( "Source Code Pro", 12, FontWeight.Bold );
            exportDisplayButton.Locked = true;

            auxPowerButton = new Button( this );
            auxPowerButton.Text = "AUX OFF";
            auxPowerButton.TextStyle = new TextStyle( "Source Code Pro", 12, FontWeight.Bold );
            auxPowerButton.OnClick += AuxPowerButton_OnClick;
            auxPowerButton.ToggleOnClick = true;

            storageView = new EHVoltageView( this, Properties.Settings.Default.NumberOfVoltagePoints );

            sensorView = new EHPowerView( this, Properties.Settings.Default.NumberOfPowerPoints );
            sensorView.Title = "Energy out";

            harvesterView = new EHPowerView( this, Properties.Settings.Default.NumberOfPowerPoints );
            harvesterView.Title = "Energy in";

            serialTimer = new Timer( OnPlotTimer );
            serialTimer.Interval = 1 * 10;

            storageUpdates = Properties.Settings.Default.StorageUpdateInterval / 10;

            if ( EHProgram.NC == false )
            {
                OpenPort();
            }
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            var client = Client;

            var storageViewHeight = 300;
            var exportBarHeight = 30;

            auxPowerButton.Area = Rectangle.FromXYWH( client.Left, client.Bottom - storageViewHeight - exportBarHeight, 100, storageViewHeight );

            exportButton.Area = Rectangle.FromXYWH( client.Left, client.Bottom - exportBarHeight, 100, exportBarHeight );
            exportDisplayButton.Area = Rectangle.FromLTRB( client.Left + 100, client.Bottom - exportBarHeight, client.Right, client.Bottom );

            storageView.Area = Rectangle.FromXYWH( auxPowerButton.Area.Right, client.Bottom - storageViewHeight - exportBarHeight, client.Width - 100, storageViewHeight );

            sensorView.Area = client.Adjust( -client.Width / 2, -storageViewHeight - exportBarHeight ).Move( client.Width / 2, 0 );
            harvesterView.Area = client.Adjust( -client.Width / 2, -storageViewHeight - exportBarHeight ).Move( 0, 0 );
        }

        protected override bool OnClosing()
        {
            ClosePort();

            return base.OnClosing();
        }

        void ExportButton_OnClick()
        {

        }

        void AuxPowerButton_OnClick()
        {
            sentDriving = false;
            driving = !driving;

            auxPowerButton.Text = auxPowerButton.Toggled ? "AUX ON" : "AUX OFF";
        }

        void WorkSerialPort()
        {
            serialTimer.Stop();

            while ( serialPort.BytesToRead > 0 && serialPort.BytesToRead % serialReadBuffer.Length == 0 )
            {
                var read = serialPort.Read( serialReadBuffer, 0, serialReadBuffer.Length );

                Debug.Assert( read == serialReadBuffer.Length );

                // Decode serial data.
                for ( var i = 0; i < 4; i++ )
                {
                    serialNumericBuffer[ i ] = 0;
                    serialNumericBuffer[ i ] |= serialReadBuffer[ i * 2 + 0 ];
                    serialNumericBuffer[ i ] |= serialReadBuffer[ i * 2 + 1 ] << 8;
                }

                // Calculate current into the sensor from the voltage across the sense resistor.
                var senseR = Properties.Settings.Default.OutSenseResistance;
                var senseLowU = (float)Math.Round( serialNumericBuffer[ 1 ] / 1000f, 5 );
                var senseHighU = (float)Math.Round( serialNumericBuffer[ 0 ] / 1000f, 5 );
                var senseU = Math.Max( 0, senseHighU - senseLowU );
                var senseP = (senseU * senseU) / senseR;

                // Calculate sensor voltage, current, power.
                var sensorU = senseHighU;
                var sensorI = (float)Math.Round( Math.Max( senseU / senseR, 0 ), 5 );
                var sensorP = (float)Math.Min( (sensorU * sensorI) + senseP, 0.012 );

                // Add another data point to the averages.
                sensorBuffer[ 0 ][ sensorBufferHead ] = sensorP;
                sensorBuffer[ 1 ][ sensorBufferHead ] = sensorU;
                sensorBuffer[ 2 ][ sensorBufferHead ] = sensorI;
                sensorBufferHead = (sensorBufferHead + 1) % sensorBufferAverages;

                // Calculate averages and adjust scales.
                var averageP = (int)Math.Round( sensorBuffer[ 0 ].Average() * 1000 * 1000 );
                var averageU = (int)Math.Round( sensorBuffer[ 1 ].Average() * 1000 );
                var averageI = (int)Math.Round( sensorBuffer[ 2 ].Average() * 1000 * 1000 );

                storageUpdateCounter += 1;

                if ( storageUpdateCounter % storageUpdates == 0 )
                {
                    var c = 0.0047;
                    var highU = (averageU / 1000.0);
                    var lowU = 2.0;

                    var lowP = Math.Max( 0, (int)Math.Round( (1 / (2 * 0.01) * c * (Math.Pow( highU, 2 ) - Math.Pow( lowU, 2 ))) * 1000 ) );
                    var highP = Math.Max( 0, (int)Math.Round( (1 / (2 * 0.5) * c * (Math.Pow( highU, 2 ) - Math.Pow( lowU, 2 ))) * 1000 ) );
                    var lowI = Math.Max( 0, (int)Math.Round( ((c * (highU - lowU)) / 0.001) * 1000 ) );
                    var highI = Math.Max( 0, (int)Math.Round( ((c * (highU - lowU)) / 0.010) * 1000 ) );

                    storageView.Annotation = $"Constant power/current available when storage is allowed to drop to 2 V\n\n" +
                        $"10  mW for {lowP.ToString( "D5" )} ms\n" +
                        $"500 mW for {highP.ToString( "D5" )} ms\n" +
                        $"1   mA for {lowI.ToString( "D5" )} ms\n" +
                        $"10  mA for {highI.ToString( "D5" )} ms";

                    storageView.Add( averageU );
                    storageView.Invalidate();
                }

                sensorView.Add( averageP, averageI, averageU );
                sensorView.Invalidate();

                senseR = Properties.Settings.Default.InSenseResistance;
                senseLowU = (float)Math.Round( serialNumericBuffer[ 2 ] / 1000f, 5 );
                senseHighU = (float)Math.Round( serialNumericBuffer[ 3 ] / 1000f, 5 );
                senseU = Math.Max( 0, senseHighU - senseLowU );
                senseP = (senseU * senseU) / senseR;

                // Calculate sensor voltage, current, power.
                sensorU = senseHighU;
                sensorI = (float)Math.Round( Math.Max( senseU / senseR, 0 ), 5 );
                sensorP = (float)Math.Min( (sensorU * sensorI) + senseP, 0.012 );

                // Add another data point to the averages.
                harvesterBuffer[ 0 ][ harvesterBufferHead ] = sensorP;
                harvesterBuffer[ 1 ][ harvesterBufferHead ] = sensorU;
                harvesterBuffer[ 2 ][ harvesterBufferHead ] = sensorI;
                harvesterBufferHead = (harvesterBufferHead + 1) % harvesterBufferAverages;

                averageP = (int)Math.Round( harvesterBuffer[ 0 ].Average() * 1000 * 1000 );
                averageU = (int)Math.Round( harvesterBuffer[ 1 ].Average() * 1000 );
                averageI = (int)Math.Round( harvesterBuffer[ 2 ].Average() * 1000 * 1000 );

                harvesterView.Add( averageP, averageI, averageU );
                harvesterView.Invalidate();
            }

            serialTimer.Start();
        }

        void OnSerialPort( object sender, SerialDataReceivedEventArgs e )
        {
            InvokeAsync( (Action)WorkSerialPort );
        }
    }

    class EHProgram
    {
        public static bool NC;

        static void OnUnhandledException( Exception exception )
        {
            Console.WriteLine( exception.ToString() );
            Console.ReadLine();
        }

        static void Main( string[] args )
        {
            NC = args.Contains( "-NC" );

            try
            {
                if ( NC == false )
                {
                    var serialPort = new SerialPort( Properties.Settings.Default.SerialPortName, 115200, Parity.None, 8, StopBits.One );
                    serialPort.Open();
                    System.Threading.Thread.Sleep( 100 );
                    serialPort.DiscardInBuffer();
                    serialPort.Close();
                }

                using ( var manager = new WindowManager( OnUnhandledException ) )
                {
                    var windowSettings = new WindowSettings();
                    windowSettings.Name = "Energy Harvesting Monitor";
                    windowSettings.Area = Rectangle.FromXYWH( 100, 100, 500, 500 );
                    var window = new EHWindow( manager, windowSettings );
                    manager.Run( window );
                }
            }
            catch ( Exception exception )
            {
                OnUnhandledException( exception );
            }
        }
    }
}
