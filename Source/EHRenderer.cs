﻿using System;
using System.Collections.Generic;

using OxyPlot;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace EHMonitor
{
    class EHPlotView : Control, IPlotView
    {
        readonly Object invalidateLock = new Object();
        readonly Object modelLock = new Object();
        readonly Object renderingLock = new Object();

        EHRenderer renderer;

        protected PlotModel Model
        {
            get;
            private set;
        }

        bool isModelInvalidated;
        bool updateDataFlag = true;

        IPlotController defaultController;

        public EHPlotView( Window window ) : base( window )
        {
            renderer = new EHRenderer();
            Model = new PlotModel();
            ((IPlotModel)Model).AttachPlotView( this );
        }

        public bool AllowInput
        {
            get;
            set;
        }

        public bool ShowLegend
        {
            get { return Model.IsLegendVisible; }
            set { Model.IsLegendVisible = value; }
        }

        public OxyThickness Padding
        {
            get { return Model.Padding; }
            set { Model.Padding = value; }
        }

        protected override void OnDraw( Context context )
        {
            renderer.Context = context;

            SharedBrush.Color = Style.Colors.Background;
            context.FillRectangle( new Rectangle( Area.Dimensions ), SharedBrush );

            lock ( invalidateLock )
            {
                if ( isModelInvalidated )
                {
                    if ( Model != null )
                    {
                        ((IPlotModel)Model).Update( updateDataFlag );
                        updateDataFlag = false;
                    }

                    isModelInvalidated = false;
                }
            }

            lock ( renderingLock )
            {
                if ( Model != null )
                {
                    ((IPlotModel)Model).Render( renderer, Area.Width, Area.Height );
                }
            }

            SharedBrush.Color = Style.Colors.Outline;
            context.DrawRectangle( new Rectangle( Area.Dimensions ), SharedBrush, 2 );
        }

        protected override void OnMouseEnter( Mouse mouse )
        {
            ActualController.HandleMouseEnter( this, mouse.ToOxyMouse() );
        }

        protected override void OnMouseUp( Mouse mouse )
        {
            if ( !AllowInput )
                return;

            ActualController.HandleMouseUp( this, mouse.ToOxyMouse() );
        }

        protected override void OnMouseMove( Mouse mouse )
        {
            if ( !AllowInput )
                return;

            ActualController.HandleMouseMove( this, mouse.ToOxyMouse() );
        }

        protected override void OnMouseLeave( Mouse mouse )
        {
            if ( !AllowInput )
                return;

            ActualController.HandleMouseLeave( this, mouse.ToOxyMouse() );
        }

        protected override void OnMouseDown( Mouse mouse )
        {
            if ( !AllowInput )
                return;

            var oxyMouseDown = mouse.ToOxyMouseDown();
            ActualController.HandleMouseDown( this, oxyMouseDown );
        }

        protected override void OnMouseWheel( Mouse mouse )
        {
            if ( !AllowInput )
                return;

            base.OnMouseWheel( mouse );
            ActualController.HandleMouseWheel( this, mouse.ToOxyMouseWheel() );
        }

        public IPlotController ActualController
        {
            get { return (defaultController ?? (defaultController = new PlotController())); }
        }

        public OxyRect ClientArea
        {
            get { return Area.ToOxy(); }
        }

        public void SetCursorType( CursorType cursorType ) { }
        public void ShowZoomRectangle( OxyRect rectangle ) { }
        public void HideZoomRectangle() { }

        PlotModel IPlotView.ActualModel => Model;

        void IPlotView.InvalidatePlot( bool updateData )
        {
            lock ( invalidateLock )
            {
                isModelInvalidated = true;
                updateDataFlag = updateDataFlag || updateData;
            }

            Refresh();
        }

        void IPlotView.SetClipboardText( String text ) { }
        void IPlotView.ShowTracker( TrackerHitResult trackerHitResult ) { }
        void IPlotView.HideTracker() { }

        Model IView.ActualModel => Model;
        IController IView.ActualController => ActualController;

        OxyRect IView.ClientArea => Area.ToOxy();

        void IView.HideZoomRectangle() { }
        void IView.SetCursorType( CursorType cursorType ) { }
        void IView.ShowZoomRectangle( OxyRect rectangle ) { }
    }

    class EHRenderer : RenderContextBase
    {
        Context drawingContext;
        SolidColorBrush drawingBrush;

        public Context Context
        {
            get { return drawingContext; }
            set
            {
                if ( drawingContext != value )
                {
                    drawingContext = value;
                    drawingBrush = new SolidColorBrush( drawingContext );
                }
            }
        }

        public override void DrawRectangle( OxyRect rect, OxyColor fill, OxyColor stroke, Double thickness )
        {
            var r = rect.ToKodo();

            drawingBrush.Color = fill.ToKodo();
            drawingContext.FillRectangle( r, drawingBrush );
            drawingBrush.Color = stroke.ToKodo();
            drawingContext.DrawRectangle( r, drawingBrush, (float)thickness );
        }

        public override void DrawRectangles( IList<OxyRect> rectangles, OxyColor fill, OxyColor stroke, Double thickness )
        {
            for ( var i = 0; i < rectangles.Count; i++ )
            {
                var r = rectangles[ i ].ToKodo();

                drawingBrush.Color = fill.ToKodo();
                drawingContext.FillRectangle( r, drawingBrush );
                drawingBrush.Color = stroke.ToKodo();
                drawingContext.DrawRectangle( r, drawingBrush, (float)thickness );
            }
        }

        public override void DrawEllipses( IList<OxyRect> rectangles, OxyColor fill, OxyColor stroke, Double thickness )
        {
            var ellipse = new Ellipse();
            var rect = new OxyRect();

            for ( var i = 0; i < rectangles.Count; i++ )
            {
                rect = rectangles[ i ];

                ellipse.Center = rect.Center.ToKodo();
                ellipse.RadiusX = (float)(rect.Width / 2);
                ellipse.RadiusY = (float)(rect.Height / 2);

                drawingBrush.Color = fill.ToKodo();
                drawingContext.FillEllipse( ellipse, drawingBrush );
                drawingBrush.Color = stroke.ToKodo();
                drawingContext.DrawEllipse( ellipse, drawingBrush, (float)thickness );
            }
        }

        public override void DrawEllipse( OxyRect rect, OxyColor fill, OxyColor stroke, Double thickness )
        {
            var ellipse = new Ellipse();
            ellipse.Center = rect.Center.ToKodo();
            ellipse.RadiusX = (float)(rect.Width / 2);
            ellipse.RadiusY = (float)(rect.Height / 2);

            drawingBrush.Color = fill.ToKodo();
            drawingContext.FillEllipse( ellipse, drawingBrush );
            drawingBrush.Color = stroke.ToKodo();
            drawingContext.DrawEllipse( ellipse, drawingBrush, (float)thickness );
        }

        public override void DrawLine( IList<ScreenPoint> points, OxyColor stroke, double thickness, double[] dashArray, OxyPlot.LineJoin lineJoin, bool aliased )
        {
            using ( var path = new PathGeometry() )
            {
                using ( var sink = path.Open() )
                {
                    sink.BeginFigure( points[ 0 ].ToKodo(), FigureBegin.Hollow );

                    for ( var i = 1; i < points.Count; i++ )
                    {
                        sink.AddLine( points[ i ].ToKodo() );
                    }

                    sink.EndFigure( FigureEnd.Open );
                    sink.Close();
                }

                if ( aliased )
                    drawingContext.AntialiasMode = AntialiasMode.Aliased;
                else
                    drawingContext.AntialiasMode = AntialiasMode.PerPrimitive;

                drawingBrush.Color = stroke.ToKodo();
                drawingContext.DrawGeometry( path, drawingBrush, (float)thickness );
            }
        }

        public override void DrawLineSegments( IList<ScreenPoint> points, OxyColor stroke, double thickness, double[] dashArray, OxyPlot.LineJoin lineJoin, bool aliased )
        {
            using ( var path = new PathGeometry() )
            {
                using ( var sink = path.Open() )
                {
                    for ( var i = 0; i < points.Count; i++ )
                    {
                        sink.BeginFigure( points[ i++ ].ToKodo(), FigureBegin.Hollow );
                        sink.AddLine( points[ i ].ToKodo() );
                        sink.EndFigure( FigureEnd.Open );
                    }

                    sink.Close();
                }

                if ( aliased )
                    drawingContext.AntialiasMode = AntialiasMode.Aliased;
                else
                    drawingContext.AntialiasMode = AntialiasMode.PerPrimitive;

                drawingBrush.Color = stroke.ToKodo();
                drawingContext.DrawGeometry( path, drawingBrush, (float)thickness );
            }
        }

        public override void DrawPolygon( IList<ScreenPoint> points, OxyColor fill, OxyColor stroke, double thickness, double[] dashArray, OxyPlot.LineJoin lineJoin, bool aliased )
        {
            using ( var path = new PathGeometry() )
            {
                using ( var sink = path.Open() )
                {
                    sink.BeginFigure( points[ 0 ].ToKodo(), FigureBegin.Filled );

                    for ( var i = 1; i < points.Count; i++ )
                    {
                        sink.AddLine( points[ i ].ToKodo() );
                    }

                    sink.EndFigure( FigureEnd.Closed );
                    sink.Close();
                }

                if ( aliased )
                    drawingContext.AntialiasMode = AntialiasMode.Aliased;
                else
                    drawingContext.AntialiasMode = AntialiasMode.PerPrimitive;

                drawingBrush.Color = fill.ToKodo();
                drawingContext.FillGeometry( path, drawingBrush );
                drawingBrush.Color = stroke.ToKodo();
                drawingContext.DrawGeometry( path, drawingBrush, (float)thickness );
            }
        }

        public override void DrawText( ScreenPoint p, string text, OxyColor fill, string fontFamily, double fontSize, double fontWeight, double rotate, HorizontalAlignment halign, VerticalAlignment valign, OxySize? maxSize )
        {
            if ( String.IsNullOrEmpty( fontFamily ) )
            {
                fontFamily = "Segoe UI";
            }

            var actualFontWeight = fontWeight < 700 ? FontWeight.Normal : FontWeight.Bold;

            using ( var context = new TextStyle( fontFamily, (float)fontSize, actualFontWeight ) )
            {
                drawingBrush.Color = fill.ToKodo();

                context.Alignment = TextStyleAlignment.LeftTop;
                var metrics = context.GetTextMetrics( text );

                var layoutX = (float)p.X;
                var layoutY = (float)p.Y;
                var layoutArea = new Rectangle( 0, 0, metrics.Width + 0.1f, metrics.Height + 0.1f );

                layoutArea = layoutArea.Move( layoutX, layoutY );

                var dx = 0f;
                if ( halign == HorizontalAlignment.Center )
                    dx = -metrics.Width / 2;
                if ( halign == HorizontalAlignment.Right )
                    dx = -metrics.Width;

                var dy = 0f;
                if ( valign == VerticalAlignment.Middle )
                    dy = -metrics.Height / 2;
                if ( valign == VerticalAlignment.Bottom )
                    dy = -metrics.Height;

                layoutArea = layoutArea.Move( dx, dy );

                Matrix3x2 transformStore = Matrix3x2.Identity;

                if ( rotate != 0 )
                {
                    transformStore = drawingContext.GetTransform();
                    drawingContext.SetTransform( Matrix3x2.Rotation( (float)rotate, layoutArea.Center ) * drawingContext.GetTransform() );
                }

                if ( maxSize.HasValue )
                {
                    drawingContext.DrawText( text, context, Rectangle.FromXYWH( (float)p.X, (float)p.Y, (float)maxSize.Value.Width, (float)maxSize.Value.Height ), drawingBrush );
                }
                else
                {
                    drawingContext.DrawText( text, context, layoutArea, drawingBrush );
                }

                if ( rotate != 0 )
                {
                    drawingContext.SetTransform( transformStore );
                }
            }
        }

        public override OxySize MeasureText( string text, string fontFamily, double fontSize, double fontWeight )
        {
            if ( String.IsNullOrEmpty( text ) )
                return OxySize.Empty;

            var actualFontWeight = fontWeight < 700 ? FontWeight.Normal : FontWeight.Bold;

            using ( var context = new TextStyle( fontFamily, (float)fontSize, actualFontWeight ) )
            {
                var metrics = context.GetTextMetrics( text );
                return new OxySize( metrics.Width, metrics.Height );
            }
        }
    }
}
