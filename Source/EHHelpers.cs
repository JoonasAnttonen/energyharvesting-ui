﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OxyPlot;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace EHMonitor
{
    public static class EHHelpers
    {
        public static ScreenPoint ToOxy(this Point point)
            => new ScreenPoint(point.X, point.Y);

        public static OxyRect ToOxy(this Rectangle rect)
            => new OxyRect(rect.Left, rect.Top, rect.Width, rect.Height);

        public static Rectangle ToKodo(this OxyRect rect)
            => Rectangle.FromXYWH((float)rect.Left, (float)rect.Top, (float)rect.Width, (float)rect.Height);

        public static Point ToKodo(this ScreenPoint point)
            => new Point((float)point.X, (float)point.Y);

        public static Color ToKodo(this OxyColor color)
            => new Color(color.A / 255f, color.R / 255f, color.G / 255f, color.B / 255f);

        public static OxyColor ToOxy(this Color color)
            => OxyColor.FromArgb((byte)(color.Alpha * 255f), (byte)(color.Red * 255f), (byte)(color.Green * 255f), (byte)(color.Blue * 255f));

        public static OxyMouseDownEventArgs ToOxyMouseDown(this Mouse mouse)
        {
            var result = new OxyMouseDownEventArgs();
            result.Position = mouse.Position.ToOxy();
            result.ClickCount = 1;

            switch (mouse.Button)
            {
                case MouseButton.Left: result.ChangedButton = OxyMouseButton.Left; break;
                case MouseButton.Middle: result.ChangedButton = OxyMouseButton.Middle; break;
                case MouseButton.Right: result.ChangedButton = OxyMouseButton.Right; break;
                case MouseButton.X1: result.ChangedButton = OxyMouseButton.XButton1; break;
                case MouseButton.X2: result.ChangedButton = OxyMouseButton.XButton2; break;
                default: result.ChangedButton = OxyMouseButton.None; break;
            }

            return result;
        }

        public static OxyMouseWheelEventArgs ToOxyMouseWheel(this Mouse mouse)
        {
            var result = new OxyMouseWheelEventArgs();
            result.Position = mouse.Position.ToOxy();
            result.Delta = mouse.WheelDelta;
            return result;
        }

        public static OxyMouseEventArgs ToOxyMouse(this Mouse mouse)
        {
            var result = new OxyMouseEventArgs();
            result.Position = mouse.Position.ToOxy();
            return result;
        }
    }
}
